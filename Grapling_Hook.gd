extends Spatial

func fire_grapling():
	
	var ray = $Ray_Cast
	var pos = Vector3()
	
	ray.force_raycast_update()
	
	if ray.is_colliding():
		pos = ray.get_collision_point()
		$Hook.global_transform.origin = pos
		$Hook.show()
		return ray.get_collision_point()
	else:
		return
	
func un_grap():
	$Hook.hide()